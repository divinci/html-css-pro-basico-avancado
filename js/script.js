// $(function () {
// Abre e fecha menu
// $('.nav-toogle, .nav-close').click(function (e) {
//   // Evita redirecionamento ao executar o click
//   e.preventDefault();
//   $('.nav').toggleClass('active');
// });
// Fixar header
// $(window).scroll(function () {
//   if ($(this).scrollTop() > 100) {
//     $('.header').addClass('fixed');
//   } else {
//     $('.header').removeClass('fixed');
//   }
// });
// });

// Navbar toggle menu
var navToggle = document.getElementsByClassName('nav-toggle')[0];
var navClose = document.getElementsByClassName('nav-close')[0];
navToggle.onclick = function (e) {
  e.preventDefault(); // evitando recarregamento após click
  document.getElementsByClassName('nav')[0].classList.toggle('active')
};

navClose.onclick = function (e) {
  e.preventDefault(); // evitando recarregamento após click
  document.getElementsByClassName('nav')[0].classList.toggle('active')
};

// Navbar style change when scroll down
var scrollPos = window.scrollY;
var header = document.querySelector('header');
var headerHeight = header.offsetHeight;

window.addEventListener('scroll', function () {
  scrollPos = window.scrollY;
  if (scrollPos >= headerHeight) {
    header.classList.add('fixed');
  } else {
    header.classList.remove('fixed');
  }
});